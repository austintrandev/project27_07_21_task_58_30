package com.example.demo;

public class Professor extends Person {
	public Professor(int age, String gender, String name, Address address, int salary) {
		super(age, gender, name, address);
		this.salary = salary;
	}

	public Professor() {
		this.salary = 20000000;
	}

	private int salary;

	public void teaching() {
		System.out.println("Professor is teaching");
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}

}
